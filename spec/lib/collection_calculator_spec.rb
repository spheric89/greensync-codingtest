require 'spec_helper'

describe CollectionCalculator do
  subject(:collection_calculator) { described_class.new(values) }
  let(:values) { [5, 10, 15, 20, 25] }

  describe '#min' do
    subject(:min) { collection_calculator.min }
    it { is_expected.to eq 5 }
  end

  describe '#max' do
    subject(:max) { collection_calculator.max }
    it { is_expected.to eq 25 }
  end

  describe '#average' do
    subject(:average) { collection_calculator.average }
    it { is_expected.to eq 15 }
  end

  describe '#median' do
    subject(:median) { collection_calculator.median }
    it { is_expected.to eq 15 }
  end

  describe '#variance' do
    subject(:variance) { collection_calculator.variance }
    it { is_expected.to eq 62.5 }
  end

  describe '#standard_deviation' do
    subject(:standard_deviation) { collection_calculator.standard_deviation }
    it { is_expected.to eq 7.9057 }
  end

  describe '#length' do
    subject(:length) { collection_calculator.length }
    it { is_expected.to eq 5 }
  end
end
