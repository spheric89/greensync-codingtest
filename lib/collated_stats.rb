# Copyright (c) 2015 GreenSync Pty Ltd.  All rights reserved.
class CollatedStats
  def initialize(time_series)
    @time_series = time_series
  end

  def values_for_day(midnight)
    (1..48).map do |interval|
      @time_series[midnight + interval * 30 * 60]
    end
  end

  def each
    @time_series.years.each do |year|
      timestamps = timestamps_for_year(year)
      yield *output_row('Year', midnight(timestamps.keys.sort.first), timestamps.values)

      @time_series.months(year).each do |month|
        timestamps = timestamps_for_month(year, month)
        yield *output_row('Month', midnight(timestamps.keys.sort.first), timestamps.values)

        midnight = timestamps.keys.first - 30 * 60
        while midnight < timestamps.keys.last
          yield *output_row('Day', midnight, values_for_day(midnight))
          midnight += 24 * 60 * 60
        end
      end
    end
  end

  def output_row(calendar_type, midnight, values)
    values = CollectionCalculator.new(values)
    [calendar_type, midnight, values.min, values.max,
     values.average, values.median, values.variance, values.standard_deviation]
  end

  private

  def timestamps_for_month(year, month)
    @time_series.month(year, month)
  end

  def timestamps_for_year(year)
    @time_series.year(year)
  end

  def midnight(timestamp)
    timestamp - 30 * 60
  end
end
