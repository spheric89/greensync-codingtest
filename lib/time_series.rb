# Copyright (c) 2015 GreenSync Pty Ltd.  All rights reserved.
class TimeSeries
  def initialize
    @data = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
  end

  def empty?
    @data.values.flat_map(&:values).flat_map(&:values).compact.empty?
  end

  def first_timestamp
    timestamps.min
  end

  def last_timestamp
    timestamps.max
  end

  def year(year)
    @data.fetch(year, {}).values.inject { |memo, obj| memo.merge(obj) } || {}
  end

  def years
    @data.keys.sort
  end

  def months(year)
    @data.fetch(year, {}).keys.sort
  end

  def month(year, month)
    @data.fetch(year, {}).fetch(month, {})
  end

  def [](timestamp)
    year, month = handle_if_midnight(timestamp)
    value = @data[year][month][timestamp]
    return nil if value.nil? || value.is_a?(Hash) && value.empty?
    value
  end

  def []=(timestamp, value)
    raise 'timestamp must be a Time' unless timestamp.is_a?(Time)
    raise 'timestamp must be UTC' unless timestamp.utc?

    year, month = handle_if_midnight(timestamp)

    if value.nil?
      @data[year][month].delete(timestamp)
    else
      @data[year][month][timestamp] = value
    end
  end

  private

  def timestamps
    @data.values.flat_map(&:values).flat_map(&:keys)
  end

  # Put midnight in correct year and month hash
  def handle_if_midnight(timestamp)
    timestamp -= 31
    [timestamp.year, timestamp.month]
  end
end
