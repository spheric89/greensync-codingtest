class CollectionCalculator
  def initialize(values)
    self.values = values
  end

  attr_reader :values

  def min
    @min ||= values.min
  end

  def max
    @max ||= values.max
  end

  def average
    @average ||= (values.inject(&:+) / values.length.to_f).round
  end

  def median
    @median ||= (values[(length - 1) / 2] + values[length / 2]) / 2.0
  end

  def variance
    @variance ||= (values.map do |value|
      result = value - average
      result * result
    end.inject(:+) / (length - 1).to_f).round(4)
  end

  def standard_deviation
    @standard_deviation ||= Math.sqrt(variance).round(4)
  end

  def length
    @length ||= values.length
  end

  private

  attr_writer :values
end
